import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Iemployee } from './Iemployee';
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/observable/throw';
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  public teacherMessageSource = new Subject();
  teacherMessage$ = this.teacherMessageSource.asObservable(); 
  
  constructor(private http : HttpClient) { }

  sendMessage(message : string) {
    this.teacherMessageSource.next(message);
  }

  colourPreference : string = "yellow";

  private _url : string = "/assets/data/employee.json";
  getDetails() : Observable<Iemployee> {
    return this.http.get<Iemployee>(this._url).pipe(catchError(err => {throw err.message || "server error"}));
                    // .catch(this.errorHandler);
  }

  errorHandler(error : HttpErrorResponse) {
    return Observable.throw(error.message || "server error")
  }
}
