import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'formsDemo';
  profile : FormGroup;
  name: string = "bharathy";

  constructor(public fb: FormBuilder) {

  }

  ngOnInit() {
    this.profile = this.fb.group({
      firstName: [''],
      lastName: ['']
    });
  }
  onSubmit(){
    console.log(this.profile);
  }
}
