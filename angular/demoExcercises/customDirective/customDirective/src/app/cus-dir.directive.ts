import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appCusDir]'
})
export class CusDirDirective {

  constructor(private el : ElementRef) {
    el.nativeElement.style.backgroundColor = "black";
    el.nativeElement.style.color = "white";
   }

}
