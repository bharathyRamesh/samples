import { Component, OnInit, Input, Output, EventEmitter, OnChanges, DoCheck } from '@angular/core';

@Component({
  selector: 'app-child-comp1',
  templateUrl: './child-comp1.component.html',
  styleUrls: ['./child-comp1.component.css']
})
export class ChildComp1Component implements OnInit, OnChanges, DoCheck {

  @Input()  childName : string;

  @Output() childEvent = new EventEmitter();

  emittedEvent() {
    this.childEvent.emit("hai this is the response from child");
  }

  // childName : string;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(){

  }

  ngDoCheck(){
    
  }

}
