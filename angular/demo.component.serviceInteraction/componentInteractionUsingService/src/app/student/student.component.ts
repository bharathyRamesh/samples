import { Component, OnInit } from '@angular/core';
import { InteractionService } from '../interaction.service';
import { Iemployee } from '../Iemployee';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  public employee : Iemployee;
  public error : string;
  constructor(private _interactionService : InteractionService) { }

  ngOnInit() {
    this._interactionService.teacherMessage$.subscribe(message => {
          if(message === "good morning student") {
            alert("good morning teacher");
          } else if (message === "well done student") {
            alert("thank you teacher");
          }
    });

  }
  getdetails() {
    this._interactionService.getDetails().subscribe(data =>  this.employee = data , err => this.error = err);
  }

  set colour(value : string) {
    this._interactionService.colourPreference = value;
  }

  get colour() {
    return this._interactionService.colourPreference;
  }
 onClick() {
   this._interactionService.colourPreference = "green";
 }

}
