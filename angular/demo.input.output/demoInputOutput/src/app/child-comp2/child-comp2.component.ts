import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child-comp2',
  templateUrl: './child-comp2.component.html',
  styleUrls: ['./child-comp2.component.css']
})
export class ChildComp2Component implements OnInit {

   @Input() child2 : string;

   @Output() childEvent2 = new EventEmitter();

   emittedEvent() {
     this.childEvent2.emit("hai this is from child 2")
   }
  
  constructor() { }

  ngOnInit() {
  }

}
