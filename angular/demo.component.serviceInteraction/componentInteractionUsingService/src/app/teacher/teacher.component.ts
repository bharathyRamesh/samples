import { Component, OnInit } from '@angular/core';
import { InteractionService } from '../interaction.service';

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  constructor(private _interactionService : InteractionService) { }

  ngOnInit() {
  }

  greetStudent() {
    this._interactionService.sendMessage("good morning student");
  }

  appreciateStudent() {
    this._interactionService.sendMessage("well done student")
  }

  set colour(value : string) {
    this._interactionService.colourPreference = value;
  }

  get colour() {
    return this._interactionService.colourPreference;
  }
}
