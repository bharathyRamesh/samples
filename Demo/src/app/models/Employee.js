var Employee = /** @class */ (function () {
    // setId(id) {
    //     this.id = id;
    // }
    // getId() {
    //     return this.id;
    // }
    function Employee(id, name) {
        this.id = id;
        this.name = name;
    }
    // id: number;
    // name: string;
    Employee.prototype.employeeDetails = function (emp) {
        console.log(emp.id);
    };
    return Employee;
}());
var e = new Employee(4, "sdf");
// e.setId(3);
e.employeeDetails(e);
// console.log(e.getId());
// let emp =  function() { console.log("hi"); } // this here we can do it as lambda function
// interface feilds {
//     x : number,
//     y :string
// }
// let emp = (variables: feilds) => {
//     console.log(variables.x);
//     console.log(variables.y);
// }
// emp(
//     {
//         x: 1,
//         y: "bharathy"
//     })
